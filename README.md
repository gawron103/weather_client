# Weather Client


![Optional Text](./img/img1.png)

# About
Simple console app which allows you to check the weather for a given city in the world on a selected day. [Weather World Online](https://www.worldweatheronline.com/) was used as a weather service. It provides to user the following informations:
- current temperature in celcius degree
- current temperature in farenheit degree
- min temperature in celcius for requested day
- min temperature in farenheit for requested day
- max temperature in celcius for requested day
- min temperature in farenheit for requested day
- current wind speed in km/h
- current wind speed in MPH
- current air pressure

# Tech
**Development:**
- Plan Java

**Unit tests:**
 - JUnit 4

# How to use it
Simply `git clone` repo to your local storage and import it to eclipse as maven project.
Program expects that the parameters for the city and dates will be given at the entrance.
So for this create run configuration like this:
`q=city date=yyyy-mm-dd`

If you don't want to use eclipse then simply compile program using `javac main.java` and run `java main`, also remember to specify input paramaters.

# Todos
**Code refactor:**
- more readable code
- better error handling

**Other:**
- more unit tests
- GUI
- perhaps port to free weather service
