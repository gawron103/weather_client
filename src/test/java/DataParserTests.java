import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.junit.BeforeClass;
import org.junit.Test;

public class DataParserTests {

	private static DataParser parser = null;
	private static WeatherInfo weather = null;
	
	@BeforeClass
	public static void init() {
		parser = new DataParser();
		
		weather = new WeatherInfo();
		
		weather.setCityName("Warszawa");
		weather.setTemperatureC("9");
		weather.setTemperatureF("35");
		weather.setMaxTempC("17");
		weather.setMinTempC("5");
		weather.setMaxTempF("14");
		weather.setMinTempF("3");
		weather.setDate("2010-04-21");
		weather.setWindSpeedMiles("7");
		weather.setWindSpeedKmh("11");
		weather.setPressure("1020");
	}
	
	@Test
	public void checkGettingData() {
		final String testXmlData = 
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + 
				"<data>\n" + 
				"  <request>\n" + 
				"    <type>LatLon</type>\n" + 
				"    <query>Warszawa</query>\n" + 
				"  </request>\n" + 
				"  <current_condition>\n" + 
				"    <observation_time>09:49 PM</observation_time>\n" + 
				"    <temp_C>9</temp_C>\n" + 
				"    <temp_F>35</temp_F>" +
				"    <weatherCode>113</weatherCode>\n" + 
				"    <windspeedMiles>7</windspeedMiles>\n" + 
				"    <windspeedKmph>11</windspeedKmph>\n" + 
				"    <humidity>50</humidity>\n" + 
				"    <visibility>10</visibility>\n" + 
				"    <pressure>1020</pressure>\n" + 
				"    <cloudcover>1</cloudcover>\n" + 
				"  </current_condition>\n" + 
				"  <weather>\n" + 
				"    <date>2010-04-21</date>\n" + 
				"    <maxtempC>17</maxtempC>\n" + 
				"    <mintempC>5</mintempC>\n" + 
				"    <maxtempF>14</maxtempF>\n" + 
				"    <mintempF>3</mintempF>\n" + 
				"  </weather>\n" + 
				"</data>";
		
		InputStream stream = new ByteArrayInputStream(testXmlData.getBytes());
		
		final WeatherInfo actualWeather = parser.parseData(stream);
		
		
		assertEquals(weather.toString(), actualWeather.toString());
	}

}
