import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import org.junit.BeforeClass;
import org.junit.Test;

//
// Response codes for HttpURLConnection
// Response is valid 200
// Response is not valid -1
//

public class ServiceConnectionTests {

	private static ServiceConnection connection;
	final int responseOK = 200;
	final int responseNotValid = -1;
	
	@BeforeClass
	public static void init() {
		connection = new ServiceConnection();
	}
	
	@Test
	public void checkIfConnectionClosedBeforeConnect() {
		final String tstString = "Test String";
		InputStream stream = null;
		
		try {
			stream = new ByteArrayInputStream(tstString.getBytes("UTF-8"));
		} 
		catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		int connectionStatus = connection.closeConnection(stream);
		
		assertEquals(responseNotValid, connectionStatus);
	}
	
	@Test
	public void checkIfStreamNullafterConnectionToNotValidService() {
		InputStream stream = null;
		InputStream expectedStream = null;
		
		int connectionStatus = connection.connect("http://dfg675.com");
		
		stream = connection.getDataFromService();
		
		assertEquals(stream, expectedStream);
	}
	
	@Test
	public void checkIfConnectedToValidService() {
		int connectionStatus = connection.connect("http://google.com");
		
		assertEquals(responseOK, connectionStatus);
	}
	
	@Test
	public void checkIfStreamNotNullAfterConnection() {
		InputStream stream = null;
		InputStream notExpectedStream = null;
		
		int responseCode = connection.connect("http://google.com");
		
		if (responseCode == responseOK) {
			stream = connection.getDataFromService();
		}
		
		assertNotEquals(notExpectedStream, stream);
	}
	
	@Test
	public void checkIfConnectionClosedAfterConnect() {
		final String tstString = "Test String";
		InputStream stream = null;
		
		try {
			stream = new ByteArrayInputStream(tstString.getBytes("UTF-8"));
		} 
		catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		int connectionStatus = connection.closeConnection(stream);
		
		assertEquals(responseOK, connectionStatus);
	}
	
	@Test
	public void checkIfConnectedToNotValidService() {
		int connectionStatus = connection.connect("http://dfg675.com");
		
		assertEquals(responseNotValid, connectionStatus);
	}
}
