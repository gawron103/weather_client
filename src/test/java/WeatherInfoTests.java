import static org.junit.Assert.*;

import java.lang.reflect.Field;

import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
import org.junit.Test;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class WeatherInfoTests {
	
	private static WeatherInfo weatherData = null;
	
	@BeforeClass
	public static void initTestClass() {
		weatherData = new WeatherInfo();
		
		weatherData.setCityName("Szczecin");
		weatherData.setTemperatureC("33");
		weatherData.setTemperatureF("270");
		weatherData.setMaxTempC("35");
		weatherData.setMinTempC("20");
		weatherData.setMaxTempF("300");
		weatherData.setMinTempF("250");
		weatherData.setDate("2019-02-01");
		weatherData.setWindSpeedMiles("60");
		weatherData.setWindSpeedKmh("100");
		weatherData.setPressure("1020");
	}

	private String getValueForTesting(String classMember) {
		String valueFromClass = "";
		
		try {
			Field field;
			field = WeatherInfo.class.getDeclaredField(classMember);
			field.setAccessible(true);
			valueFromClass = (String)field.get(weatherData);
		} 
		catch (NoSuchFieldException e) {
			e.printStackTrace();
		} 
		catch (SecurityException e) {
			e.printStackTrace();
		} 
		catch (IllegalArgumentException e) {
			e.printStackTrace();
		} 
		catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		
		return valueFromClass;
	}
	
	@Test
	public void checkIfCorrectCityStored() {
		final String actualValue = getValueForTesting("cityName");
		
		assertEquals("Szczecin", actualValue);
	}
	
	@Test
	public void checkIfCorrectTempCStored() {
		final String actualValue = getValueForTesting("temperatureC");
		
		assertEquals("33", actualValue);
	}
	
	@Test
	public void checkIfCorrectTempFStored() {
		final String actualValue = getValueForTesting("temperatureF");
		
		assertEquals("270", actualValue);
	}
	
	@Test
	public void checkIfCorrectMaxTempCStored() {
		final String actualValue = getValueForTesting("maxTempC");
		
		assertEquals("35", actualValue);
	}
	
	@Test
	public void checkIfCorrectMinTempCStored() {
		final String actualValue = getValueForTesting("minTempC");
		
		assertEquals("20", actualValue);
	}
	
	@Test
	public void checkIfCorrectMaxTempFStored() {
		final String actualValue = getValueForTesting("maxTempF");
		
		assertEquals("300", actualValue);
	}
	
	@Test
	public void checkIfCorrectMinTempFStored() {
		final String actualValue = getValueForTesting("minTempF");
		
		assertEquals("250", actualValue);
	}
	
	@Test
	public void checkIfCorrectDateStored() {
		final String actualValue = getValueForTesting("date");
		
		assertEquals("2019-02-01", actualValue);
	}
	
	@Test
	public void checkIfCorrectWindSpeedMilesStored() {
		final String actualValue = getValueForTesting("windSpeedMiles");
		
		assertEquals("60", actualValue);
	}
	
	@Test
	public void checkIfCorrectWindSpeedKmhStored() {
		final String actualValue = getValueForTesting("windSpeedKilometers");
		
		assertEquals("100", actualValue);
	}
	
	@Test
	public void checkIfCorrectPressureStored() {
		final String actualValue = getValueForTesting("pressure");
		
		assertEquals("1020", actualValue);
	}
	
	@Test
	public void checkTempPrint() {
		final String actualValue = weatherData.toString();
		final String expectedValue = "Weather for city:\t"
				+ "Szczecin" + "\n" +
				"for:\t" +
				"2019-02-01" + "\n" +
				"Current temp in celcius:\t" +
				"33" + "\n" +
				"Current temp in farenheit:\t" +
				"270" + "\n" +
				"Minimum temperature in celcius:\t" +
				"20" + "\n" +
				"Maximum temperature in celcius:\t" +
				"35" + "\n" +
				"Minimum temperature in farenheit:\t" +
				"250" + "\n" +
				"Maximum temperature in farenheit:\t" +
				"300" + "\n" +
				"Wind speed in kilometers:\t" +
				"100" + "\n" +
				"Wind speed in miles:\t" +
				"60" + "\n" +
				"Pressure:\t" + 
				"1020" + "\n";
		
		assertEquals(expectedValue, actualValue);
	}	
	
	@Test
	public void checkWhenWeatherIsEmpty() {
		weatherData.setCityName("");
		weatherData.setTemperatureC("");
		weatherData.setTemperatureF("");
		weatherData.setMaxTempC("");
		weatherData.setMinTempC("");
		weatherData.setMaxTempF("");
		weatherData.setMinTempF("");
		weatherData.setDate("");
		weatherData.setWindSpeedMiles("");
		weatherData.setWindSpeedKmh("");
		weatherData.setPressure("");
		
		assertEquals("Invalid data", weatherData.toString());
	}
}