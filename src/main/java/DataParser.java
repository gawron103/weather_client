import java.io.InputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;


/**
 * 
 * @author piotrekgawronski
 *  Class responsible for parsing data
 *  retrieved from the service.
 */
public class DataParser implements IfDataParser {
	private DocumentBuilderFactory dbf;
	private DocumentBuilder db;
	private Document doc;

	public DataParser() {
		try {
			dbf = DocumentBuilderFactory.newInstance();
			db = dbf.newDocumentBuilder();
		}
		catch(Exception e) {
			System.out.println("Data parser constructor error");
		}
	}
	
	/**
	 * Main parsing function which gets every needed data
	 * @param data
	 * @return
	 */
	public WeatherInfo parseData(InputStream data) {
		WeatherInfo weather = new WeatherInfo();
		
		try {
			doc = db.parse(data);

			final String cityName = getDataFor("request", "query");
			weather.setCityName(cityName); 

			final String tempC = getDataFor("current_condition", "temp_C");
			weather.setTemperatureC(tempC);

			final String tempF = getDataFor("current_condition", "temp_F");
			weather.setTemperatureF(tempF);

			final String windSpeedM = getDataFor("current_condition", "windspeedMiles");
			weather.setWindSpeedMiles(windSpeedM);

			final String windSpeedKmh = getDataFor("current_condition", "windspeedKmph");
			weather.setWindSpeedKmh(windSpeedKmh);

			final String pressure = getDataFor("current_condition", "pressure");
			weather.setPressure(pressure);

			final String date = getDataFor("weather", "date");
			weather.setDate(date);

			final String maxTempC = getDataFor("weather", "maxtempC");
			weather.setMaxTempC(maxTempC);

			final String minTempC = getDataFor("weather", "mintempC");
			weather.setMinTempC(minTempC);

			final String maxTempF = getDataFor("weather", "maxtempF");
			weather.setMaxTempF(maxTempF);

			final String minTempF = getDataFor("weather", "mintempF");
			weather.setMinTempF(minTempF);
		}
		catch(Exception e) {
			System.out.println(e);
		}
		
		return weather; 
	}

	/**
	 * Helper method for getting requested data from xml
	 * @param data
	 * @param element
	 * @return
	 */
	public String getDataFor(String data, String element) {
		NodeList nList = doc.getElementsByTagName(data);

		Node nNode = nList.item(0);
		Element eElement = (Element)nNode;

		return eElement.getElementsByTagName(element).item(0).getTextContent();
	}
}
