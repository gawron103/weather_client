import java.io.InputStream;

public class Main {
	
	/**
	 * Function responsible for parsing users input
	 * in order to create request
	 * @param input
	 * @return
	 */
	private static String transformUserInput(String[] input) {
		String flagsSet = "";
		
		for(String flag : input) {
			if(flag.contains("q=")) {
				flagsSet += "&" + flag;
			}
			else if(flag.contains("date=")) {
				flagsSet += "&" + flag;
			}
			else {
				//input not valid, do nothing
			}
		}
		
		return flagsSet;
	}

	/**
	 * Helper function responsible for showing supported
	 * flags. Called when users input it not valid.
	 */
	private static void showHelp() {
		System.out.print("Supported flags:\n\n"
				+ "q=XXX\t\t\t where XXX is city name\n"
				+ "date=yyyy-mm-dd\t\t get weather for specific day\n"
				+ "num_of_days=X\t\t where X is number of wanted days\n\n");
	}
	
	/**
	 * Main function of the program
	 * @param args
	 */
	public static void main(String[] args) {
		final String usersNeeds = transformUserInput(args);
		
		if (usersNeeds.isEmpty() || !usersNeeds.contains("q=")) {
			System.out.println("Not valid request");
			showHelp();
			return;
		}
		
		ServiceConnection serviceConnection = new ServiceConnection();;
		DataParser parser = new DataParser();
		InputStream data = null;
		
		final String base_url = "http://api.worldweatheronline.com/premium/v1/weather.ashx?";
		final String key = "key=1e6dca9effd64758983155158181812";
		final String wanted_file_format = "&format=xml";
		
		String url = base_url + key + usersNeeds + wanted_file_format;
		
		int responseFromService = serviceConnection.connect(url);
		
		if (responseFromService == -1) {
			System.out.println("Cannot connect to the service");
			return;
		}
		
		data = serviceConnection.getDataFromService();
		
		if (null == data) {
			System.out.println("Data is null");
		}
		
		WeatherInfo weatherInfo = parser.parseData(data);
		
		System.out.println(weatherInfo);
		
		responseFromService = serviceConnection.closeConnection(data);
		
		if (responseFromService == -1) {
			System.out.println("Cannot close connection");
		}
	}
}