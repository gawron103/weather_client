import java.io.InputStream;

public interface IfServiceConnection {
	public abstract int connect(String inputUrl);
	public abstract int closeConnection(InputStream stream);
	public abstract InputStream getDataFromService();
}
