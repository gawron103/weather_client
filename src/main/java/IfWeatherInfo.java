public interface IfWeatherInfo {
	public abstract void setCityName(String cityName);
	public abstract void setTemperatureC(String temperatureC);
	public abstract void setTemperatureF(String temperatureF);
	public abstract void setMaxTempC(String temperatureC);
	public abstract void setMinTempC(String temperatureC);
	public abstract void setMaxTempF(String temperatureF);
	public abstract void setMinTempF(String temperatureF);
	public abstract void setDate(String date);
	public abstract void setWindSpeedMiles(String wind);
	public abstract void setWindSpeedKmh(String wind);
	public abstract void setPressure(String pressure);
	public abstract String toString();
}
