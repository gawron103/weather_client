import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 * 
 * @author piotrekgawronski
 *	Class responsible for handling connection with 
 *  World Weather Online service. 
 */
public class ServiceConnection implements IfServiceConnection{
	
	private HttpURLConnection httpConnection;
	private URL url;
	
	/**
	 * Method responsible for creating connection 
	 * to the service
	 */
	public int connect(String inputUrl) {
		int responseCode = -1;
		
		try {
			url = new URL(inputUrl);
			
			httpConnection = (HttpURLConnection)url.openConnection();
			responseCode = httpConnection.getResponseCode();
			System.out.println("Opened connection");
		}
		catch(Exception e) {
			System.out.println("Connection failed");
		}
		
		return responseCode;
	}
	
	/**
	 * Method responsible for closing connection.
	 * @param stream
	 */
	public int closeConnection(InputStream stream) {
		int responseCode = -1;
		
		try {
			stream.close();
			httpConnection.disconnect();
			responseCode = 200;
			System.out.println("Connection closed");
		} 
		catch (Exception e) {
			System.out.println("Failed to close connection");
		}
		
		return responseCode;
	}
	
	/**
	 * Method responsible for getting requested data
	 * from service. Given data depends on URL.
	 * @return
	 */
	public InputStream getDataFromService() {
		try {
			return httpConnection.getInputStream();
		}
		catch(Exception e) {
			System.out.println("Failed to get data");
		}
		
		return null;
	}
}
