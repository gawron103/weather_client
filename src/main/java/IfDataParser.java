import java.io.InputStream;

public interface IfDataParser {
	abstract public WeatherInfo parseData(InputStream data);
	abstract public String getDataFor(String data, String element);
}
