public class WeatherInfo implements IfWeatherInfo {
	private String cityName = "";
	private String temperatureC = "";
	private String temperatureF = "";
	private String maxTempC = "";
	private String minTempC = "";
	private String maxTempF = "";
	private String minTempF = "";
	private String date = "";
	private String windSpeedMiles = "";
	private String windSpeedKilometers = "";
	private String pressure = "";
	
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	
	public void setTemperatureC(String temperatureC) {
		this.temperatureC = temperatureC;
	}
	
	public void setTemperatureF(String temperatureF) {
		this.temperatureF = temperatureF;
	}

	public void setMaxTempC(String temperatureC) {
		this.maxTempC = temperatureC;
	}

	public void setMinTempC(String temperatureC) {
		this.minTempC = temperatureC;
	}
	
	public void setMaxTempF(String temperatureF) {
		this.maxTempF = temperatureF;
	}

	public void setMinTempF(String temperatureF) {
		this.minTempF = temperatureF;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public void setWindSpeedMiles(String wind) {
		this.windSpeedMiles = wind;
	}

	public void setWindSpeedKmh(String wind) {
		this.windSpeedKilometers = wind;
	}

	public void setPressure(String pressure) {
		this.pressure = pressure;
	}

	@Override
	public String toString() {
		// The assumption is that if city is empty, then
		// the whole data is invalid and there should be
		// output which could inform user about that 
		if ("" == cityName) {
			return "Invalid data";
		}
		else {
			return "Weather for city:\t"
					+ cityName + "\n" +
					"for:\t" +
					date + "\n" +
					"Current temp in celcius:\t" +
					temperatureC + "\n" +
					"Current temp in farenheit:\t" +
					temperatureF + "\n" +
					"Minimum temperature in celcius:\t" +
					minTempC + "\n" +
					"Maximum temperature in celcius:\t" +
					maxTempC + "\n" +
					"Minimum temperature in farenheit:\t" +
					minTempF + "\n" +
					"Maximum temperature in farenheit:\t" +
					maxTempF + "\n" +
					"Wind speed in kilometers:\t" +
					windSpeedKilometers + "\n" +
					"Wind speed in miles:\t" +
					windSpeedMiles + "\n" +
					"Pressure:\t" + 
					pressure + "\n";
		}
	}
}
